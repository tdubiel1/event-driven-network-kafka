# event-driven-network-kafka
The Feb24th Ansible4Networking meetup demo uses playbooks/rulebooks from this repo. This code does not cover the scope of confguring the AAP controller.  "job_template: "Cisco-Config-Management"
Those bits will be covered in a future meetup demo.

## Getting started
The following links will explain how to install EDA and the other components used in the demo.

### Event-Driven Ansible install:
Please note this code is the developer tech preview.
https://github.com/ansible/event-driven-ansible

### Telegraf Collector Install
https://docs.influxdata.com/telegraf/v1.21/introduction/installation/

Please see this repo for my example telegraf.toml file.

### Kafka Install
https://kafka.apache.org/quickstart

### Cisco router CSR1000v IOS-XE
This filter triggers an event whenever a rtr config is changed on rtr1.
~~~
telemetry ietf subscription 2
 encoding encode-kvgpb
 filter xpath /ios:native
 source-address <rtr1 IP>
 stream yang-push
 update-policy on-change
 receiver ip address <telegraf IP> 57000 protocol grpc-tcp
~~~

## Run Demo (Kafka)
Run this from the terminal of the EDA server install.
~~~
ansible-rulebook --rulebook rulebooks/kafka-config-drift-rules.yml -i inventory.yml --verbose
~~~

## Cahnge the router config from SSH
~~~
conf t
<config something>
end
~~~

